<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    protected $primaryKey = 'sid';
    protected $table = 'students';

    public function Clearance() {
        return $this->hasOne(clearance::class,'sid','sid');
    }

}
