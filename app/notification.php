<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notification extends Model
{
    protected $primaryKey = 'nid';
    protected $table = 'notifications';

    public function Student() {
        return $this->belongsTo(student::class,'sid','sid');
    }

    public function User() {
        return $this->belongsTo(User::class,'uid','uid');
    }
}
