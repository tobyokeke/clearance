<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class clearance extends Model
{

    protected $table = 'clearances';
    protected $primaryKey = 'cid';
    protected $dates= ['facultyClearedAt','accountClearedAt','libraryClearedAt','registrarClearedAt','studentlifeClearedAt'];

    public function Student() {
        return $this->belongsTo(student::class,'sid','sid');
    }

}
