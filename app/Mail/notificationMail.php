<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class notificationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $content;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content,$user)
    {
        $this->content  = $content;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.notificationMailView')->subject("Update on your clearance");
    }
}
