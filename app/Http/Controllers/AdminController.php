<?php

namespace App\Http\Controllers;

use App\Mail\notificationMail;
use App\notification;
use App\student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\clearance;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function registrarDashboard() {
        $clearances = clearance::orderBy('created_at','desc')->paginate(20);
        return view('registrar.dashboard',[
            'clearances' => $clearances
        ]);
    }

    public function libraryDashboard() {
        $clearances = clearance::orderBy('created_at','desc')->paginate(20);
        return view('library.dashboard',[
            'clearances' => $clearances
        ]);
    }

    public function facultyDashboard() {
        $clearances = clearance::orderBy('created_at','desc')->paginate(20);
        return view('faculty.dashboard',[
            'clearances' => $clearances
        ]);
    }

    public function accountsDashboard() {
        $clearances = clearance::orderBy('created_at','desc')->paginate(20);
        return view('accounts.dashboard',[
            'clearances' => $clearances
        ]);
    }

    public function studentlifeDashboard() {
        $clearances = clearance::orderBy('created_at','desc')->paginate(20);
        return view('studentlife.dashboard',[
            'clearances' => $clearances
        ]);
    }

    public function studentDetails($sid) {
        $student = student::find($sid);
        return view('registrar.students.details',[
            'student' => $student
        ]);
    }


    public function approveRegistrar($sid) {

        $clearance = clearance::where('sid',$sid)->first();
        $clearance->registrarCleared = 'cleared';
        $clearance->registrarClearedAt = Carbon::now();
        $clearance->registrarSignature = auth()->user()->signature;
        $clearance->save();

        $message = "Your clearance has been completed. You can now print your clearance form.";

        $notification = new notification();
        $notification->sid = $sid;
        $notification->uid = auth()->user()->id;
        $notification->title = "Clearance Complete";
        $notification->message = $message;
        $notification->from = "Registrar";
        $notification->status = 'pending';
        $notification->save();


        session()->flash('success','Student Fully Cleared');
        return redirect()->back();

    }

    public function approveFaculty($sid) {

        $clearance = clearance::where('sid',$sid)->first();
        $clearance->facultyCleared = 'cleared';
        $clearance->facultyClearedAt = Carbon::now();
        $clearance->facultySignature = auth()->user()->signature;
        $clearance->save();

        session()->flash('success','Student Cleared');
        return redirect()->back();

    }

    public function approveLibrary($sid) {

        $clearance = clearance::where('sid',$sid)->first();
        $clearance->libraryCleared = 'cleared';
        $clearance->libraryClearedAt = Carbon::now();
        $clearance->librarySignature = auth()->user()->signature;
        $clearance->save();

        session()->flash('success','Student Cleared');
        return redirect()->back();

    }

    public function approveStudentLife($sid) {

        $clearance = clearance::where('sid',$sid)->first();
        $clearance->studentlifeCleared = 'cleared';
        $clearance->studentlifeClearedAt = Carbon::now();
        $clearance->studentlifeSignature = auth()->user()->signature;
        $clearance->save();

        session()->flash('success','Student Cleared');
        return redirect()->back();

    }


    public function approveAccounts($sid) {

        $clearance = clearance::where('sid',$sid)->first();
        $clearance->accountCleared = 'cleared';
        $clearance->accountClearedAt = Carbon::now();
        $clearance->accountSignature = auth()->user()->signature;
        $clearance->save();

        session()->flash('success','Student Cleared');
        return redirect()->back();

    }

    public function rejectAccounts(Request $request) {
        $message = $request->input('message');
        $sid = $request->input('sid');

        $clearance = clearance::where('sid',$sid)->first();
        $clearance->accountCleared = 'disabled';
        $clearance->save();

        $notification = new notification();
        $notification->sid = $sid;
        $notification->uid = auth()->user()->id;
        $notification->title = "Accounts Clearance Issue";
        $notification->message = $message;
        $notification->from = "Accounts";
        $notification->status = 'pending';
        $notification->save();

        try{
            Mail::to($clearance->Student->email)->send(new notificationMail($message,$clearance->Student));
        }catch (\Exception $exception){}


        session()->flash('success','Student Rejected.');
        return redirect()->back();

    }

    public function rejectStudentLife(Request $request) {
        $message = $request->input('message');
        $sid = $request->input('sid');

        $clearance = clearance::where('sid',$sid)->first();
        $clearance->accountCleared = 'disabled';
        $clearance->save();

        $notification = new notification();
        $notification->sid = $sid;
        $notification->uid = auth()->user()->id;
        $notification->title = "Student Life Clearance Issue";
        $notification->message = $message;
        $notification->from = "StudentLife";
        $notification->status = 'pending';
        $notification->save();


        try{
            Mail::to($clearance->Student->email)->send(new notificationMail($message,$clearance->Student));
        }catch (\Exception $exception){}


        session()->flash('success','Student Rejected.');
        return redirect()->back();

    }

    public function rejectFaculty(Request $request) {
        $message = $request->input('message');
        $sid = $request->input('sid');


        $clearance = clearance::where('sid',$sid)->first();
        $clearance->facultyCleared = 'disabled';
        $clearance->save();

        $notification = new notification();
        $notification->sid = $sid;
        $notification->uid = auth()->user()->id;
        $notification->title = "Faculty Clearance Issue";
        $notification->message = $message;
        $notification->from = "Faculty";
        $notification->status = 'pending';
        $notification->save();

        try{
            Mail::to($clearance->Student->email)->send(new notificationMail($message,$clearance->Student));
        }catch (\Exception $exception){}

        session()->flash('success','Student Rejected.');
        return redirect()->back();

    }

    public function rejectLibrary(Request $request) {
        $message = $request->input('message');
        $sid = $request->input('sid');

        $clearance = clearance::where('sid',$sid)->first();
        $clearance->libraryCleared = 'disabled';
        $clearance->save();

        $notification = new notification();
        $notification->sid = $sid;
        $notification->uid = auth()->user()->id;
        $notification->title = "Library Clearance Issue";
        $notification->message = $message;
        $notification->from = "Library";
        $notification->status = 'pending';
        $notification->save();

        try{
            Mail::to($clearance->Student->email)->send(new notificationMail($message,$clearance->Student));

        }catch (\Exception $exception){}

        session()->flash('success','Student Rejected.');
        return redirect()->back();

    }

    public function uploadSignature(Request $request) {
        if($request->hasFile('signature')){
            $filename = Str::random(5) . $request->file('signature')->getClientOriginalName();

            $request->file('signature')->move("uploads",$filename);

            $url = url('uploads/' . $filename);

            $user = User::find(auth()->user()->id);
            $user->signature = $url;
            $user->save();
            session()->flash('success','Signature Uploaded.');
            return redirect()->back();
        }

        else {
            session()->flash('error','No file uploaded.');
            return redirect()->back();
        }

    }

}
