<?php

namespace App\Http\Controllers;

use App\student;
use Illuminate\Http\Request;

class PublicController extends Controller
{

    public function index()
    {
        if(session()->has('student')) return redirect()->route('students.dashboard');
        if(auth()->check()) return redirect()->route('home');
        return view('welcome');
    }

    public function studentLogin() {
        if(session()->has('student')) return redirect()->route('students.dashboard');
        return view('students.login');
    }

    public function postStudentLogin(Request $request) {

        $email = $request->input('email');
        $password = $request->input('password');

        if(student::where('email',$email)->count() <= 0 ) {
            session()->flash('error','Email does not exist');
            return redirect()->back();
        }

        $student = student::where('email',$email)->first();

        if(password_verify($password,$student->password)){
            session()->put('student',$student);
            session()->put('isLoggedIn',true);
            return redirect()->route('students.dashboard');
        } else{
            session()->flash('error','Wrong password');
            return redirect()->back();
        }

    }

    public function staffLogin()
    {
        return view('staff-login');
    }
}
