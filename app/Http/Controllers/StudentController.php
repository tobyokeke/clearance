<?php

namespace App\Http\Controllers;

use App\notification;
use App\student;
use Illuminate\Http\Request;
use App\clearance;
use App\User;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('student');
    }

    public function dashboard()
    {
        $notificationCount = notification::where('sid',session()->get('student')->sid)->where('status','pending')->count();
        return view('students.dashboard',
            [
                'student' => session()->get('student'),
                'notificationCount' => $notificationCount
            ]);
    }


    public function clearance()
    {
        $clearanceCount = clearance::where('sid',session()->get('student')->sid)->count();
        return view('students.clearance',[
            'clearanceCount' => $clearanceCount
        ]);
    }

    public function startClearance(Request $request) {
        $sid = $request->input('sid');

        $clearance = new clearance();
        $clearance->sid = $sid;
        $clearance->save();

        session()->flash('success','Clearance Started.');
        return redirect()->route('students.dashboard');
    }

    public function postUploadClearance(Request $request){

        try{

            $clearance = new clearance();
            $clearance->studentid = $request->input('studentid');
            $clearance->studentName = $request->input('studentName');
            $clearance->studentSurname = $request->input('studentSurname');
            $clearance->faculty = $request->input('faculty');
            $clearance->department = $request->input('department');
            $clearance->programme = $request->input('programme');
            $clearance->level = $request->input('level');
            $clearance->save();


            $request->session()->flash('success','clearance Added.');

            return redirect('upload-clearance');

        }catch (\Exception $exception){

            $request->session()->flash('error','Sorry an error occurred. Please try again');
            return redirect( 'upload-clearance');

        }


    }

    public function notifications() {
        $notifications = notification::orderBy('created_at','desc')->where('sid',session()->get('student')->sid)->get();
        return view('students.notifications',[
            'notifications' => $notifications
        ]);
    }

    public function resolveNotification($nid) {
        $clearance = clearance::where('sid',session()->get('student')->sid)->first();

        $notification = notification::find($nid);
        $notification->status = 'resolved';
        $notification->save();

        if(strtolower($notification->from) == 'accounts'){
            $clearance->accountCleared = 'pending';
            $clearance->save();
        }

        if(strtolower($notification->from) == 'faculty'){
            $clearance->facultyCleared = 'pending';
            $clearance->save();
        }

        if(strtolower($notification->from) == 'library'){
            $clearance->libraryCleared = 'pending';
            $clearance->save();
        }

        if(strtolower($notification->from) == 'studentlife'){
            $clearance->studentlifeCleared = 'pending';
            $clearance->save();
        }

//        if(strtolower($notification->from) == 'registrar'){
//            $clearance->registrarCleared = 'pending';
//            $clearance->save();
//        }

        session()->flash('success','Notification Resolved.');
        return redirect()->back();

    }

    public function print(student $student) {
        return view('students.print',[
            'student' => $student
        ]);
    }

    public function logout() {
        session()->remove('student');
        return redirect('/');
    }

}
