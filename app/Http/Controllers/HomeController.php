<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        if(auth()->user()->role == 'registrar') return redirect()->route('registrars.dashboard');
        if(auth()->user()->role == 'library') return redirect()->route('library.dashboard');
        if(auth()->user()->role == 'accounts') return redirect()->route('accounts.dashboard');
        if(auth()->user()->role == 'studentlife') return redirect()->route('studentlife.dashboard');
        if(auth()->user()->role == 'faculty') return redirect()->route('faculty.dashboard');

    }

    public function dashboard() {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function logout() {
        auth()->logout();
        return redirect('/');
    }
}
