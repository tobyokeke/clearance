<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class student
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( session()->has('student')){
            session()->put('student',\App\student::find(session()->get('student')->sid));
            return $next($request);
        }
        else{
            $request->session()->flash("error","You are not authorized to access that page.");
            return redirect("/");
        }
    }
}
