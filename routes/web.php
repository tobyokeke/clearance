<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'PublicController@index');

Auth::routes();

Route::get('/students/login','PublicController@studentLogin')->name('students.login');
Route::post('/students/login','PublicController@postStudentLogin')->name('students.login.post');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/welcome', 'PublicController@index')->name('welcome');
Route::get('/staff-login', 'PublicController@staffLogin')->name('staff-login');
Route::get('/upload-clearance','StudentController@uploadClearance')->name('upload-clearance');
Route::post('/upload-clearance','StudentController@postUploadClearance');
Route::post('/student-dashboard','StudentController@studentDashboard')->name('student-dashboard');



Route::group(['prefix' => 'students'],function(){
    Route::get('dashboard','StudentController@dashboard')->name('students.dashboard');
    Route::get('clearance','StudentController@clearance')->name('students.clearance');
    Route::get('notifications','StudentController@notifications')->name('students.notifications');

    Route::get('print/{student}','StudentController@print')->name('students.print');
    Route::get('notifications/resolve/{nid}','StudentController@resolveNotification')->name('students.notifications.resolve');
    Route::get('logout','StudentController@logout')->name('students.logout');

    Route::post('clearance','StudentController@startClearance')->name('students.clearance.start.post');
});


Route::group(['prefix' => 'registrar'],function(){
    Route::get('dashboard','AdminController@registrarDashboard')->name('registrars.dashboard');
    Route::get('student/{sid}','AdminController@studentDetails')->name('registrars.student.details');

    Route::get('approve/{sid}','AdminController@approveRegistrar')->name('registrars.approve');

});

Route::group(['prefix' => 'faculty'],function(){
    Route::get('dashboard','AdminController@facultyDashboard')->name('faculty.dashboard');

    Route::get('approve/{sid}','AdminController@approveFaculty')->name('faculty.approve');
    Route::post('reject','AdminController@rejectFaculty')->name('faculty.reject');

});

Route::group(['prefix' => 'library'],function(){
    Route::get('dashboard','AdminController@libraryDashboard')->name('library.dashboard');

    Route::get('approve/{sid}','AdminController@approveLibrary')->name('library.approve');
    Route::post('reject','AdminController@rejectLibrary')->name('library.reject');

});

Route::group(['prefix' => 'studentlife'],function(){
    Route::get('dashboard','AdminController@studentlifeDashboard')->name('studentlife.dashboard');

    Route::get('approve/{sid}','AdminController@approveStudentLife')->name('studentlife.approve');
    Route::post('reject','AdminController@rejectStudentLife')->name('studentlife.reject');

});

Route::group(['prefix' => 'accounts'],function(){
    Route::get('dashboard','AdminController@accountsDashboard')->name('accounts.dashboard');

    Route::get('approve/{sid}','AdminController@approveAccounts')->name('accounts.approve');
    Route::post('reject','AdminController@rejectAccounts')->name('accounts.reject');
});

Route::post('upload-signature','AdminController@uploadSignature')->name('upload.signature');


Route::get('logout','HomeController@logout')->name('logout');





