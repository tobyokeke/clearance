@extends('accounts.layouts.app')

@section('content')
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>


    <div class="container">

        <div>
            <h3>Accounts Department</h3>
            <h4><b>{{auth()->user()->name}}</b></h4>
            <h5>Accounts Officer</h5>
        </div>

        <div>

            <label>Your Current Signature: </label>
            <img src="{{auth()->user()->signature}}" style="width:200px;">

            <form method="post" enctype="multipart/form-data" action="{{route('upload.signature')}}">

                @csrf
                <input type="file" name="signature">
                <button class="btn btn-success">Upload</button>
            </form>
        </div>

        <h3 align="center">Students for clearance</h3>

        <p>
            Logged in as {{auth()->user()->name}} ({{auth()->user()->role}})
        </p>

        <table class="table table-striped">
            <tr>
                <th>S/N</th>
                <th>Student ID</th>
                <th>Student Name</th>
                <th>Date</th>
                <th></th>
            </tr>

            @foreach($clearances as $clearance)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$clearance->Student->studentid}}</td>
                    <td>{{$clearance->Student->name}} {{$clearance->Student->surname}}</td>
                    <td>{{$clearance->created_at->toDayDateTimeString()}}</td>
                    <td>

                        @if($clearance->accountCleared == 'pending')
                        <a href="{{route('accounts.approve',['sid' => $clearance->Student->sid])}}" style="padding: 10px" class="badge badge-success ">Approve</a>

                        <!-- Button trigger modal -->
                        <button id="disapprove" type="button" class="btn btn-danger"  data-sid="{{$clearance->sid}}" onclick="setSid()" data-toggle="modal" data-target="#rejectModal">
                            Disapprove
                        </button>
                        @endif

                        @if($clearance->accountCleared == 'disabled')
                            <span class="badge badge-danger">Rejected</span>
                        @endif

                        @if($clearance->accountCleared == 'cleared')
                            <span class="badge badge-success">Cleared</span>
                        @endif

                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    <script>
        function setSid(){
            var disapprove = document.getElementById('disapprove');
            console.log(disapprove.dataset.sid);
            var sid = document.getElementById('sid');
            sid.value = disapprove.dataset.sid;
        }
    </script>


    <!-- Modal -->
    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" action="{{route('accounts.reject')}}">
                    @csrf
                    <input type="hidden" id="sid" name="sid">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Message for student</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea name="message" class="form-control" placeholder="Enter your message here"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Reject</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

