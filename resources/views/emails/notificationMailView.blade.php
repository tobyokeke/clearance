Good day {{$user->name}},

You have a new notification in your clearance portal. Please login to resolve it. Message is below:
<br><br><br><br>

{{$content}}


<br><br><br><br><br><br>

If you have any issues. Please contact the registrar.
