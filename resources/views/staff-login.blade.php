@extends('layouts.app')

@section('content')
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
    <div class="container"><br><br><br><br><br>

        <div class="row" style="margin-left: 120px;">
            <div class="col-md-6">
                <div class="card" style="width: 18rem; background-color: #1b4b72; color:white;">
                    <div class="card-body">
                        <a href="{{url('login')}}">
                            <h5 class="card-title" style="text-align: center; color: white;">Login as Registrar</h5>
                        </a>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" style="width: 18rem; background-color: #1b4b72; color:white;">
                    <div class="card-body">
                        <a href="{{url('login')}}">
                            <h5 class="card-title" style="color: white; text-align: center;"> Login as Staff </h5>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

