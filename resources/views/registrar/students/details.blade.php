@extends('registrar.layouts.app')

@section('content')


    <div class="container">

        <div class="row" style="border: 1px solid black; border-bottom: none">
            <div class="col-md-4">
                <label class="col-md-6">Student No</label>
                <span>
                    {{$student->studentid}}
                </span>
            </div>

            <div class="col-md-4">
                <label class="col-md-6">Name</label>
                <span>
                    {{$student->name}}
                </span>
            </div>

            <div class="col-md-4">
                <label class="col-md-6">Surname</label>
                <span>
                    {{$student->surname}}
                </span>
            </div>
        </div>


        <div class="row" style="border: 1px solid black; border-top: none">
            <div class="col-md-4">
                <label class="col-md-6">Programme</label>
                <span>
                    {{$student->programme}}
                </span>
            </div>

            <div class="col-md-4">
                <label class="col-md-6">Level</label>
                <span>
                    {{$student->level}}
                </span>
            </div>

            <div class="col-md-4">
                <label class="col-md-6">Faculty</label>
                <span>
                    {{$student->faculty}}
                </span>
            </div>
        </div>


        <div class="row" style="border: 1px solid black; border-top: none">
            <div class="col-md-6">
                <h3>Faculty</h3>

                <table class="table table-bordered">
                    <tr>
                        <th>Year</th>
                        <th>1<br>

                            @if($student->Clearance->facultyCleared == 'cleared')
                            <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>2<br>
                            @if($student->Clearance->facultyCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>3<br>
                            @if($student->Clearance->facultyCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>4<br>
                            @if($student->Clearance->facultyCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif

                        </th>
                        <th>5<br>
                            @if($student->Clearance->facultyCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                    </tr>

                    <tr>
                        <td colspan="6">Signature & Date:

                            @if($student->Clearance->facultyCleared && isset($student->Clearance->facultyClearedAt))
                                <img style="width:100px;" src="{{$student->Clearance->facultySignature}}">
                                {{$student->Clearance->facultyClearedAt->toDayDateTimeString()}}
                            @endif
                        </td>
                    </tr>


                </table>

                @if($student->Clearance->facultyCleared == 'cleared')
                <img class="approved" src="{{url('img/approved.png')}}">
                @endif

            </div>

            <div class="col-md-6">
                <h3>Accounts</h3>

                <table class="table table-bordered">
                    <tr>
                        <th>Year</th>
                        <th>1<br>

                            @if($student->Clearance->accountCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>2<br>
                            @if($student->Clearance->accountCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>3<br>
                            @if($student->Clearance->accountCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>4<br>
                            @if($student->Clearance->accountCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif

                        </th>
                        <th>5<br>
                            @if($student->Clearance->accountCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                    </tr>

                    <tr>
                        <td colspan="6">Signature & Date:

                            @if($student->Clearance->accountCleared == 'cleared' && isset($student->Clearance->accountClearedAt))
                                <img style="width:100px;" src="{{$student->Clearance->accountSignature}}">
                                {{$student->Clearance->accountClearedAt->toDayDateTimeString()}}
                            @endif
                        </td>
                    </tr>


                </table>

                @if($student->Clearance->accountCleared == 'cleared')
                    <img class="approved" src="{{url('img/approved.png')}}">
                @endif

            </div>

            <div class="col-md-6">
                <h3>Library</h3>

                <table class="table table-bordered">
                    <tr>
                        <th>Year</th>
                        <th>1<br>

                            @if($student->Clearance->libraryCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>2<br>
                            @if($student->Clearance->libraryCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>3<br>
                            @if($student->Clearance->libraryCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>4<br>
                            @if($student->Clearance->libraryCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif

                        </th>
                        <th>5<br>
                            @if($student->Clearance->libraryCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                    </tr>

                    <tr>
                        <td colspan="6">Signature & Date:

                            @if($student->Clearance->libraryCleared == 'cleared' && isset($student->Clearance->libraryClearedAt))
                                <img style="width:100px;" src="{{$student->Clearance->librarySignature}}">
                                {{$student->Clearance->libraryClearedAt->toDayDateTimeString()}}
                            @endif
                        </td>
                    </tr>


                </table>

                @if($student->Clearance->libraryCleared == 'cleared')
                    <img class="approved" src="{{url('img/approved.png')}}">
                @endif

            </div>

            <div class="col-md-6">
                <h3>Student Life Division</h3>

                <table class="table table-bordered">
                    <tr>
                        <th>Year</th>
                        <th>1<br>

                            @if($student->Clearance->studentlifeCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>2<br>
                            @if($student->Clearance->studentlifeCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>3<br>
                            @if($student->Clearance->studentlifeCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                        <th>4<br>
                            @if($student->Clearance->studentlifeCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif

                        </th>
                        <th>5<br>
                            @if($student->Clearance->studentlifeCleared == 'cleared')
                                <img class="check" src="{{url('img/check.png')}}">
                            @endif
                        </th>
                    </tr>

                    <tr>
                        <td colspan="6">Signature & Date:

                            @if($student->Clearance->studentlifeCleared == 'cleared' && isset($student->Clearance->studentlifeClearedAt))
                            <img style="width:100px;" src="{{$student->Clearance->studentlifeSignature}}">
                            {{$student->Clearance->studentlifeClearedAt->toDayDateTimeString()}}
                            @endif
                        </td>
                    </tr>


                </table>

                @if($student->Clearance->studentlifeCleared == 'cleared')
                    <img class="approved" src="{{url('img/approved.png')}}">
                @endif

            </div>



            <div class="col-6">

                <button class="btn btn-warning" style="margin: 10px;" onclick="history.back()">Back</button>

                @if($student->Clearance->studentlifeCleared == 'cleared' && $student->Clearance->accountCleared == 'cleared' && $student->Clearance->libraryCleared == 'cleared' && $student->Clearance->facultyCleared == 'cleared' && $student->Clearance->registrarCleared == 'pending')
                    <a href="{{route('registrars.approve',[ 'sid' => $student->sid])}}" class="btn btn-success" style="margin: 10px;">Approve</a>
                    <button class="btn btn-primary" onclick="window.print()" style="margin: 10px;">Print</button>

                @endif

                @if($student->Clearance->studentlifeCleared == 'cleared' && $student->Clearance->accountCleared == 'cleared' && $student->Clearance->libraryCleared == 'cleared' && $student->Clearance->facultyCleared == 'cleared' && $student->Clearance->registrarCleared == 'cleared')
                    <button class="btn btn-primary" onclick="window.print()" style="margin: 10px;">Print</button>
                @endif

            </div>

            <div class="col-6">

                <p>Registrar Signature and Date</p>

                @if(isset($student->Clearance->registrarClearedAt))
                    <img src="{{auth()->user()->signature}}" style="width:200px;">
                    <span style="text-decoration: underline">{{$student->Clearance->registrarClearedAt->toDayDateTimeString()}}</span>
                    @else
                    <span style="text-decoration: underline">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>

                @endif
            </div>
</div>

</div>


@endsection
