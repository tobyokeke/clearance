@extends('registrar.layouts.app')

@section('content')
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>


    <div class="container">

        <div>
            <h3>Registrar Department</h3>
            <h4><b>{{auth()->user()->name}}</b></h4>
            <h5>Registrar Officer</h5>
        </div>

        <div>

            <label>Your Current Signature: </label>
            <img src="{{auth()->user()->signature}}" style="width:200px;">

            <form method="post" enctype="multipart/form-data" action="{{route('upload.signature')}}">

                @csrf
                <input type="file" name="signature">
                <button class="btn btn-success">Upload</button>
            </form>
        </div>

        <h3 align="center">Students for clearance</h3>
        <table class="table table-striped">
            <tr>
                <th>S/N</th>
                <th>Student ID</th>
                <th>Student Name</th>
                <th>Status</th>
                <th>Date</th>
                <th></th>
            </tr>

            @foreach($clearances as $clearance)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$clearance->Student->studentid}}</td>
                    <td>{{$clearance->Student->name}} {{$clearance->Student->surname}}</td>
                    <td>
                        @if($clearance->registrarCleared == 'cleared')
                            <span class="badge badge-success">Cleared</span>
                            @else
                            <span class="badge badge-danger">Un-Cleared</span>

                        @endif
                    </td>
                    <td>{{$clearance->created_at->toDayDateTimeString()}}</td>
                    <td>
                        <a href="{{route('registrars.student.details',['sid' => $clearance->Student->sid])}}" style="padding: 10px" class="badge badge-success ">View</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

