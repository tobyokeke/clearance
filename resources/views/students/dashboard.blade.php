@extends('students.layouts.app')

@section('content')
    <style>

        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .col,
        > [class*="col-"] {
            margin-right: 10px;
            margin-left: 10px;
        }


    </style>
    <div class="container">

        @if(isset($student->Clearance))
        <div class="row" align="center">
            @if($student->Clearance->facultyCleared == 'cleared')
                <div class="col-3">
                    <div class="alert gutter alert-success">
                        <h3>Faculty</h3>
                        <p class="badge badge-success">Cleared</p>
                    </div>
                </div>
            @else
                <div class="col-3">
                    <div class="alert alert-danger">
                        <h3>Faculty</h3>
                        <p class="badge badge-danger">Un-Cleared</p>
                    </div>
                </div>
            @endif

            @if($student->Clearance->libraryCleared == 'cleared')
                <div class="col-3">
                    <div class="alert alert-success">
                        <h3>Library</h3>
                        <p class="badge badge-success">Cleared</p>
                    </div>
                </div>
            @else

                <div class="col-3">
                    <div class="alert alert-danger">
                        <h3>Library</h3>
                        <p class="badge badge-danger">Un-Cleared</p>
                    </div>
                </div>

            @endif

            @if($student->Clearance->studentlifeCleared == 'cleared')
                <div class="col-3">
                    <div class="alert alert-success">
                        <h3>Student Life</h3>
                        <p class="badge badge-success">Cleared</p>
                    </div>
                </div>
            @else

                <div class="col-3">
                    <div class="alert alert-danger">
                        <h3>Student Life</h3>
                        <p class="badge badge-danger">Un-Cleared</p>
                    </div>
                </div>

            @endif

                @if($student->Clearance->accountCleared == 'cleared')
                    <div class="col-3">
                        <div class="alert alert-success">
                            <h3>Accounts</h3>
                            <p class="badge badge-success">Cleared</p>
                        </div>
                    </div>
                @else

                    <div class="col-3">
                        <div class="alert alert-danger">
                            <h3>Accounts</h3>
                            <p class="badge badge-danger">Un-Cleared</p>
                        </div>
                    </div>

                @endif

        </div>

            {{--@if($student->Clearance->studentlifeCleared == 'cleared' && $student->Clearance->accountCleared == 'cleared' && $student->Clearance->libraryCleared == 'cleared' && $student->Clearance->facultyCleared == 'cleared' && $student->Clearance->registrarCleared == 'cleared')--}}

                {{--<div align="center">--}}
                    {{--<a class="btn btn-outline-success" href="{{route('students.print',['student' => $student])}}">Print</a>--}}
                {{--</div>--}}
                {{--@else--}}
                {{--<div align="center">--}}
                    {{--<p>You would see a print button here when you are fully cleared.</p>--}}
                {{--</div>--}}
            {{--@endif--}}
        @endif

         <br><br><br><br><br>
        <h3>Student Home Screen : Welcome {{session()->get('student')->name}}
        </h3>
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card" style="width: 18rem; background-color: #1b4b72; color:white;">
                    <div class="card-body">
                        <a href="{{route('students.clearance')}}">
                            <h5 class="card-title" style="color: white; text-align: center;">Request Clearance</h5>
                        </a>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" style="width: 18rem; background-color: #1b4b72; color:white;">
                    <div class="card-body">
                        <a href="{{route('students.notifications')}}">

                            <h5 class="card-title" style="text-align: center; color: white;">
                                <span class="badge badge-danger">{{$notificationCount}}</span> &nbsp; View Notifications</h5>

                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

