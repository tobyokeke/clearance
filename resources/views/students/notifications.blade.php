@extends('students.layouts.app')

@section('content')


    <div class="container">

        <h3 align="center">Notifications</h3>
        <table class="table table-striped">
            <tr>
                <th>Title</th>
                <th>Message</th>
                <th>From</th>
                <th>Date</th>
                <th></th>
            </tr>

            @foreach($notifications as $notification)
                <tr>
                    <td>{{$notification->title}}</td>
                    <td>{{$notification->message}}</td>
                    <td>{{$notification->from}}</td>
                    <td>{{$notification->created_at->toDayDateTimeString()}}</td>
                    <td>

                        @if($notification->status == 'pending')

                            @if($notification->title == 'Clearance Complete')
                                <a class="btn btn-outline-success" href="{{route('students.print',['student' => $notification->sid])}}">Print</a>
                            @else
                                <a href="{{route('students.notifications.resolve',['nid' => $notification->nid])}}" style="padding: 10px" class="badge badge-primary ">Resolve Now</a>
                            @endif

                            @else
                            <span style="padding: 10px"  class="badge badge-success">Resolved</span>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

@endsection
