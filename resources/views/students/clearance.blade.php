@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="text-align: center">{{ __('Student Clearance Request Form') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('students.clearance.start.post') }}">
                            @csrf

                            <input type="hidden" name="sid" value="{{session()->get('student')->sid}}">
                            <div class="form-group row">
                                <label for="studentid" class="col-md-4 col-form-label text-md-right">{{ __('Student ID') }}</label>

                                <div class="col-md-6">
                                    <input id="studentid" type="text" class="form-control @error('studentid') is-invalid @enderror" name="studentid" value="{{ session()->get('student')->studentid }}" disabled required autocomplete="studentid" autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="studentname" class="col-md-4 col-form-label text-md-right">{{ __('Student Name') }}</label>

                                <div class="col-md-6">
                                    <input id="studentname" type="text" class="form-control @error('studentname') is-invalid @enderror" name="studentname" value="{{ session()->get('student')->name }}" disabled required autocomplete="studentname" autofocus>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="studentsurname" class="col-md-4 col-form-label text-md-right">{{ __('Student Surname') }}</label>

                                <div class="col-md-6">
                                    <input id="studentsurname" type="text" class="form-control @error('studentsurname') is-invalid @enderror" name="studentsurname" value="{{ session()->get('student')->surname }}" disabled required  autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="faculty" class="col-md-4 col-form-label text-md-right">Faculty</label>

                                <div class="col-md-6">
                                    <input id="studentsurname" type="text" class="form-control @error('faculty') is-invalid @enderror" name="faculty" value="{{ session()->get('student')->faculty }}" disabled required autofocus>
                                </div>

                            </div>


                            <div class="form-group row">
                                <label for="department" class="col-md-4 col-form-label text-md-right">Department</label>

                                <div class="col-md-6">
                                    <input id="studentsurname" type="text" class="form-control" name="department" value="{{ session()->get('student')->department }}" disabled required autofocus>
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="programme" class="col-md-4 col-form-label text-md-right">programme</label>

                                <div class="col-md-6">
                                    <input id="programme" type="text" class="form-control" name="programme" value="{{ session()->get('student')->programme }}" disabled required autofocus>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label for="level" class="col-md-4 col-form-label text-md-right">Level</label>

                                <div class="col-md-6">
                                    <input id="level" type="text" class="form-control" name="level" value="{{ session()->get('student')->level }}" disabled required autofocus>
                                </div>

                            </div>

                            @if($clearanceCount <= 0)
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Start Clearance
                                    </button>
                                </div>
                            </div>
                                @else

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <span class="alert alert-warning">
                                            Clearance already started. In progress
                                        </span>
                                    </div>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
