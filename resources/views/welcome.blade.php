@extends('layouts.app')

@section('content')
    <style>
        html, body {
            background-image: url("img/banner.png") !important;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }


        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
    <div class="container"  ><br><br><br><br><br>
        <div class="row" style="margin-left: 150px;">
            <div class="col-md-6">
                <div class="card" style="width: 18rem; background-color: #1f6fb2; color: white;">
                    <div class="card-body">
                        <a href="{{route('students.login')}}">
                            <h5 class="card-title" style="color: white;">Student</h5>
                        </a>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" style="width: 18rem; background-color: #1d68a7;">
                    <div class="card-body">
                        <a href="{{url('staff-login')}}">
                            <h5 class="card-title" style="color: white;">University Staff </h5>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

