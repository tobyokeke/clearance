<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDB extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email',191)->unique();
            $table->enum('role',array('registrar','accounts','library','faculty','studentlife'));
            $table->timestamp('email_verified_at')->nullable();
            $table->string('signature',5000); // store path to signature of user
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('students', function(Blueprint $table){
            $table->increments('sid');
            $table->string('studentid');
            $table->string('email',191)->unique();
            $table->string('name');
            $table->string('surname');
            $table->string('faculty');
            $table->string('department');
            $table->string('programme');
            $table->string('level');
            $table->string('password');
            $table->timestamps();

        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email',191)->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('clearances', function (Blueprint $table) {
            $table->increments('cid');
            $table->integer('sid');
            $table->enum('facultyCleared',['pending','cleared','disabled'])->default('pending');
            $table->string('facultySignature')->nullable();
            $table->timestamp('facultyClearedAt')->nullable();

            $table->enum('accountCleared',['pending','cleared','disabled'])->default('pending');
            $table->string('accountSignature')->nullable();
            $table->timestamp('accountClearedAt')->nullable();

            $table->enum('libraryCleared',['pending','cleared','disabled'])->default('pending');
            $table->string('librarySignature')->nullable();
            $table->timestamp('libraryClearedAt')->nullable();

            $table->enum('studentlifeCleared',['pending','cleared','disabled'])->default('pending');
            $table->string('studentlifeSignature')->nullable();
            $table->timestamp('studentlifeClearedAt')->nullable();

            $table->enum('registrarCleared',['pending','cleared','disabled'])->default('pending');
            $table->string('registrarSignature')->nullable();
            $table->timestamp('registrarClearedAt')->nullable();

            $table->timestamps();
        });

        Schema::create('notifications', function(Blueprint $table){
            $table->increments('nid');
            $table->integer('sid');
            $table->integer('uid');
            $table->string('title');
            $table->string('message',1000);
            $table->string('from');
            $table->enum('status',['pending','resolved'])->default('pending');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('students');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('clearances');
    }

}
